#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 17:15:50 2018

@author: daniel
@tag: esto es una prueba de branch y merge en gitlab
"""

import cv2
import numpy as np
import os
import random

dataset = []

def extraerImagenes(carpeta, label):
    for archivo in os.listdir(carpeta):
        print(os.path.join(carpeta,archivo))
        if os.path.isdir(os.path.join(carpeta,archivo)):
            label = archivo
            print(label)
            extraerImagenes(os.path.join(carpeta,archivo),label)
        else:
            extension = os.path.splitext(os.path.join(carpeta,archivo))[1]
            if extension == '.ppm':
                image = cv2.imread(os.path.join(carpeta,archivo))
                resized_image = cv2.resize(image, (28, 28)) 
                gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
                dataset.append((gray,label))
                
extraerImagenes("GTSRB_Final_Training_Images/GTSRB/Final_Training/Images/",0)

N = len(dataset)
N_train = np.round(0.6 * N).astype(np.int32)
N_val = np.round(0.2 * N).astype(np.int32)
random.shuffle(dataset)
train_data = dataset[0:N_train]
val_data = dataset[N_train:(N_train+N_val)]
test_data = dataset[(N_train+N_val):-1]






