#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  1 16:00:21 2018

@author: daniel
"""

import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range
from IPython.display import display, Image
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_mldata
from sklearn.cross_validation import train_test_split
from numpy import array
import cv2

X_train = []
Y_train = []
X_test = []
Y_test = []
X_val = []
Y_val = []
señales = ['60', '70', '80', '100', '120', 'Ceda el paso', 'Stop', 'Prohibido el paso', 'Flecha_arriba', 'Rotonda']
num_contour = 4

for sample in train_data:
    X_train.append(sample[0].flatten())
    Y_train.append(float(sample[1]))

for sample in val_data:
    X_val.append(sample[0].flatten())
    Y_val.append(float(sample[1]))

for sample in test_data:
    X_test.append(sample[0].flatten())
    Y_test.append(float(sample[1]))
    
image_size = 28
num_labels = 10
num_channels = 1

train_dataset = array(X_train)
train_labels = array(Y_train)
valid_dataset = array(X_val)
valid_labels = array(Y_val)
test_dataset = array(X_test)
test_labels = array(Y_test)

def reformat(dataset, labels):
    dataset = dataset.reshape((-1, image_size, image_size, num_channels)).astype(np.float32)
    labels = (np.arange(num_labels) == labels[:,None]).astype(np.float32)
    return dataset, labels

train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)

print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])

batch_size = 128
patch_size = 5
depth_1 = 16
depth_2 = 32
depth_3 = 64
num_hidden1 = 128
num_hidden2 = 64

graphCC = tf.Graph()

with graphCC.as_default():

    tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size, image_size, num_channels), name='x')
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels), name='y_train')
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)
  
    layer1_weights = tf.Variable(tf.truncated_normal(
          [patch_size, patch_size, num_channels, depth_1], stddev=0.1))
    layer1_biases = tf.Variable(tf.zeros([depth_1]))

    layer2_weights = tf.Variable(tf.truncated_normal(
      [patch_size, patch_size, depth_1, depth_2], stddev=0.1))

    layer2_biases = tf.Variable(tf.constant(1.0, shape=[depth_2]))
    
    layer2_2_weights = tf.Variable(tf.truncated_normal(
      [patch_size, patch_size, depth_2, depth_3], stddev=0.1))

    layer2_2_biases = tf.Variable(tf.constant(1.0, shape=[depth_3]))
    
    layer3_weights = tf.Variable(tf.truncated_normal([1024, num_hidden1], stddev=0.1)) ##Stride =2 !!

    layer3_biases = tf.Variable(tf.constant(1.0, shape=[num_hidden1]))

    layer4_weights = tf.Variable(tf.truncated_normal(
          [num_hidden1, num_hidden2], stddev=0.1))
    layer4_biases = tf.Variable(tf.constant(1.0, shape=[num_hidden2]))
    
    layer5_weights = tf.Variable(tf.truncated_normal(
          [num_hidden2, num_labels], stddev=0.1))
    layer5_biases = tf.Variable(tf.constant(1.0, shape=[num_labels]))
    
    def model(data):
        conv = tf.nn.conv2d(data, layer1_weights, [1, 2, 2, 1], padding='SAME')
        hidden = tf.nn.relu(conv + layer1_biases)
        conv = tf.nn.conv2d(hidden, layer2_weights, [1, 2, 2, 1], padding='SAME')
        hidden = tf.nn.relu(conv + layer2_biases)
        conv = tf.nn.conv2d(hidden, layer2_2_weights, [1, 2, 2, 1], padding='SAME')
        hidden = tf.nn.relu(conv + layer2_2_biases)
        shape = hidden.get_shape().as_list()
        reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
        hidden = tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases)
        out_l4 = tf.nn.relu(tf.matmul(hidden, layer4_weights) + layer4_biases)
        out_l5 = tf.matmul(out_l4, layer5_weights) + layer5_biases 
        return out_l5   
    
    logits = model(tf_train_dataset)
    loss = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels,logits=logits))
    
    optimizer = tf.train.AdamOptimizer(learning_rate=5e-03).minimize(loss) 
    
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = tf.nn.softmax(model(tf_valid_dataset))
    test_prediction = tf.nn.softmax(model(tf_test_dataset))  

num_steps = 501
session = tf.InteractiveSession(graph=graphCC)

tf.global_variables_initializer().run()
print('Initialized')
for step in range(num_steps):
    offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
    batch_data = train_dataset[offset:(offset + batch_size), :, :, :]
    batch_labels = train_labels[offset:(offset + batch_size), :]
    feed_dict = {tf_train_dataset : batch_data, tf_train_labels : batch_labels}
    _, l, predictions = session.run(
          [optimizer, loss, train_prediction], feed_dict=feed_dict)
    if (step % 100 == 0):
        print('Minibatch loss at step %d: %f' % (step, l))
        print('Minibatch accuracy: %.1f%%' % accuracy(predictions, batch_labels))
        print('Validation accuracy: %.1f%%' % accuracy(valid_prediction.eval(), valid_labels))
    _
print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
print(test_prediction)

#########################################################
#########################################################
###Cambio de fase (red convolucional a video)############
#########################################################
#########################################################
        
cap = cv2.VideoCapture('video3.mp4')
 
if (cap.isOpened() == False): 
  print("Error opening video stream or file")
 
while(cap.isOpened()):
  ret, frame = cap.read()
  if ret == True:
    aux_frame = frame
    max_y = np.size(frame, 0)
    max_x = np.size(frame, 1)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    umbral1 = np.array([90,140,50])
    umbral2 = np.array([205,225,255])
    mask = cv2.inRange(hsv, umbral1, umbral2)
#    edges = cv2.Canny(mask,600,600) #200,300
    (_, contours, hierarchy)  = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE) 
    
    frame
    
    cv2.namedWindow('signal',cv2.WINDOW_NORMAL)
    cv2.resizeWindow('signal', 200,200)
    
    x_aux = 0
    y_aux = 0
    w_aux = 0 
    h_aux = 0   
    
    for contour in contours:
        
        if num_contour != 4:
            num_contour = num_contour + 1
            continue
        
        if num_contour == 5:
            num_contour = 0
            
        x,y,w,h = cv2.boundingRect(contour)
        
        if x_aux<x<x_aux+w_aux and y_aux<y<y_aux+h_aux:
            continue
        
        if w>160 and h>160:
            continue
        
        if h>1.2*w:
            continue
        
        if w>1.12*h:
            continue
        
        if w<75 and h<75:
            continue
        
        if y>3*max_y/4 or y<max_y/4:
            continue
        
        if x<2*max_x/3 and x>max_x/3:
            continue
        
        crop_frame = aux_frame[y:y+h, x:x+w]
        x_aux, y_aux, w_aux, h_aux = x,y,w,h
            
        cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)            
        
        crop_frame_resized = cv2.resize(crop_frame, (28,28))
        gray = cv2.cvtColor(crop_frame_resized, cv2.COLOR_BGR2GRAY)
        gray.flatten()
        gray = gray.reshape((-1, image_size, image_size, num_channels)).astype(np.float32)
        tf_gray = tf.constant(gray)
        prediction = (tf.nn.softmax(model(tf_gray))).eval()
        class_pred = prediction.flatten().argsort()[::-1][0]
        print(señales[int(class_pred)])
        cv2.imshow('signal', crop_frame)

    cv2.namedWindow('image',cv2.WINDOW_NORMAL)
    cv2.resizeWindow('image', 800,800)
    cv2.imshow('image',frame)
 
    if cv2.waitKey(16) & 0xFF == ord('q'):
      break
 
  else: 
    break
 
cap.release()
 
cv2.destroyAllWindows()
    
